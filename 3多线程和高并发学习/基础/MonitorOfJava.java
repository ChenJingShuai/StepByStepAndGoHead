/**
 * 	One of strengths of the Java program language is its support for multithreading at the language level.Much of this
 * support centers on synchronization:coordinating activities and data access among multiple threads.The mechanism that 
 * Java uses to support synchronization is the monitor.
 *  Java's monitor supports two kinds of synchronization:mutual exclusion and cooperation.Mutual exclusion,which is supp-
 * orted in the JVM via object locks,enables multiple threads to independently work on shared data without interfering
 * with each other.Cooperation,which is supported in the JVM via the wait and notify(all) methods of class Object,enables
 * multiple threads to work together toward a common goal. 
 * 
 *  见火狐标签页-Java多线程系列
 *  
 * @author ChenJingShuai
 *
 * 每天进步一点-2016年4月11日-下午1:23:58
 */
public class MonitorOfJava {

}
